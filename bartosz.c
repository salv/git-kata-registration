
#include "stdio.h"

struct profile {
    char *firstname;
    char *lastname;
    char *company;
    unsigned int age;
};

void present(struct profile *p)
{
    printf("%s %s - %s\n", p->firstname, p->lastname, p->company);
}

int main(int argc, char *argv[])
{
    struct profile p;
    
    p.firstname = "Bartosz";
    p.lastname = "Ciechanowski";
    p.company = "Macoscope";
    p.age = 23;
    
    present(&p);
    
    return 0;
}

